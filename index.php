<?
/**
 * Homework
 * @version 3.0
 * 
 * Функциональное программирование
 */

require './article.php'; //Подключаем класс Article для удобной типизации

/**
 * Получает массив статей
 *
 * @return Article[]
 */
function getArticles () {
    return json_decode(file_get_contents("./data.json"));
}

/**
 * Получает заголовок статьи
 * @param integer $id Идентификатор статьи
 * @return string Заголовок статьи
 */
function getArticleTitle ($id) {  }

/**
 * Получает краткое содержание статьи
 *
 * @param integer $id Идентификатор статьи
 * @param integer $length Длина краткого содержания статьи
 * @param string $postfix Постфикс краткого содержания, добавляется в конец текста
 * @return string Краткое содержание статьи
 */
function getArticlePreviewText ($id, $length, $postfix = "...") {  }

function getArticleText ($id) {  }

function getArticleDate ($id) {  }

function getArticleLink ($id) {  }

function showPreviewArticle ($id) {  }

function showArticlesList ($articlesPerPage, $page) {  }

function showDetailArticle ($id) {  }

function showPagination ($articlesPerPage, $page) {  }

function showPostCounterChanger () {  }

$articles = getArticles();

/** 
 * 
 * @todo Предусмотреть функционал выбора количества выводимых статей
 * @todo Предусмотреть постраничную навигацию
 * @todo Задокументировать код используя стандарт PHPDoc {@link https://manual.phpdoc.org/HTMLSmartyConverter/HandS/phpDocumentor/tutorial_tags.pkg.html}
 * 
 */
